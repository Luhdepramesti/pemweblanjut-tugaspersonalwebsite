<?php

//use App\Models\home;
//use App\Models\tentangsaya;
//use App\Models\contact;
use Illuminate\Support\Facades\Route;
//use App\Http\Controllers\HomeController;
//use App\Http\Controllers\TentangSayaController;
//use App\Http\Controllers\ContactController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'home']);

Route::get('/tentangsaya', [App\Http\Controllers\TentangSayaController::class, 'tentangsaya']);

Route::get('/contact', [App\Http\Controllers\ContactController::class, 'contact']);